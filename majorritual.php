<?php

/*
 * MailDragon - majorritual.php
 * (C) Katie Moore 2020
 */

// imap_driver class is based on https://github.com/mazaika/imap_driver/blob/master/imap_driver.php and other Stack Overflow solutions.
// I would advise against tampering with it unless you know what you're doing.  It manages interacting with the IMAP inbox (which you will need to
// do in order to interface with your MailDragon Inbox to read mail for relaying)
class imap_driver
{
    private $fp;                      // file pointer
    public $error;                    // error message
	
	private $command_counter = "00000001";
    public $last_response = array();
    public $last_endline = "";
    
    public function init($host, $port)
    {
        if (!($this->fp = fsockopen($host, $port, $errno, $errstr, 15))) {
            $this->error = "Could not connect to host ($errno) $errstr";
            return false;
        }
        if (!stream_set_timeout($this->fp, 15)) {
            $this->error = "Could not set timeout";
            return false;
        }
        $line = fgets($this->fp);     // discard the first line of the stream
        return true;
    }
    
    private function close()
    {
        fclose($this->fp);
    }
	
	private function command($command)
	{
		$this->last_response = array();
		$this->last_endline  = "";
		
		fwrite($this->fp, "$this->command_counter $command\r\n");            // send the command
		
		while ($line = fgets($this->fp)) {                                   // fetch the response one line at a time
			$line = trim($line);                                             // trim the response
			$line_arr = preg_split('/\s+/', $line, 0, PREG_SPLIT_NO_EMPTY);  // split the response into non-empty pieces by whitespace
			
			if (count($line_arr) > 0) {
				$code = array_shift($line_arr);                              // take the first segment from the response, which will be the line number
				
				if (strtoupper($code) == $this->command_counter) {
					$this->last_endline = join(' ', $line_arr);              // save the completion response line to parse later
					break;
				} else {
					$this->last_response[] = $line;                          // append the current line to the saved response
				}
				
			} else {
				$this->last_response[] = $line;
			}
		}
		
		$this->increment_counter();
	}

	private function increment_counter()
	{
		$this->command_counter = sprintf('%08d', intval($this->command_counter) + 1);
	}
	
	public function login($login, $pwd)
    {
        $this->command("LOGIN $login $pwd");
        if (preg_match('~^OK~', $this->last_endline)) {
            return true;
        } else {
            $this->error = join(', ', $this->last_response);
            $this->close();
            return false;
        }
    }
	
	public function select_folder($folder)
    {
        $this->command("SELECT $folder");
        if (preg_match('~^OK~', $this->last_endline)) {
            return true;
        } else {
            $this->error = join(', ', $this->last_response);
            $this->close();
            return false;
        }
    }
	
	public function get_uids_by_search($criteria)
    {
        $this->command("SEARCH $criteria");
        
        if (preg_match('~^OK~', $this->last_endline)
        && is_array($this->last_response)
        && count($this->last_response) == 1) {
        
            $splitted_response = explode(' ', $this->last_response[0]);
            $uids              = array();
            
            foreach ($splitted_response as $item) {
                if (preg_match('~^\d+$~', $item)) {
                    $uids[] = $item;                        // put the returned UIDs into an array
                }
            }
            return $uids;
            
        } else {
            $this->error = join(', ', $this->last_response);
            $this->close();
            return false;
        }
    }
	
	public function get_headers_from_uid($uid)
    {
        $this->command("FETCH $uid BODY.PEEK[HEADER]");
        
        if (preg_match('~^OK~', $this->last_endline)) {
            array_shift($this->last_response);                  // skip the first line
            $headers    = array();
            $prev_match = '';
            
            foreach ($this->last_response as $item) {
                if (preg_match('~^([a-z][a-z0-9-_]+):~is', $item, $match)) {
                    $header_name           = strtolower($match[1]);
                    $prev_match            = $header_name;
                    $headers[$header_name] = trim(substr($item, strlen($header_name) + 1));
                    
                } else {
                    $headers[$prev_match] .= " " . $item;
                }
            }
            return $headers;
            
        } else {
            $this->error = join(', ', $this->last_response);
            $this->close();
            return false;
        }
    } 
	
	public function get_text_from_uid($uid)
    {
        $this->command("FETCH $uid BODY.PEEK[TEXT]");
		$res = "";
		
		$count = count($this->last_response);
		for ($i = 1; $i < $count - 1; $i++) {
			$res .= $this->last_response[$i] . "\r\n";
		}
		
		return $res;
    } 
	
	public function set_seen($uid)
	{
		$this->command("STORE $uid +FLAGS \Seen");
	}
}

/*
 * This code from hereon out is more modifiable!
 */

class MailDragonExtractor
{
	/*
	 * Given the headers from a mail, extract the sender's mail address without embellishment
	 * e.g. "Alice <alice@somegame.com>" would return "alice@somegame.com"
	 */ 
	public function extract_from_mail_address($headers)
	{
		$res = "";
		
		$len = strlen($headers["from"]);
		
		// We need to actively ignore everything until we hit the first '<' since that's where we'll find the mail address.
		// If no '<' exists then we must have a raw address so we can use that.
		$ignore = (strpos($headers["from"],"<"));
		
		for ($i = 0; $i < $len; $i++)
		{
			if ($ignore)
			{
				if ( $headers["from"][$i] === '<' )
				{
					$ignore = false;
				}
			}
			else if ( $headers["from"][$i] != '>' )
			{
				$res .= ( $headers["from"][$i] );
			}
		}
		
		return $res;
	}
	
	/*
	 * Given the headers from a mail, extract all recipient (IC) mail addresses as an array without embellishment
	 * e.g. "Alice <alice@somegame.com>, Beatrice <beatrice@somegame.com>" would return {"alice@somegame.com", "beatrice@somegame.com"}
	 * This will apply to both the "to" and the "cc" field.  Bcc is not yet supported.
	 */ 
	public function extract_to_mail_addresses($headers)
	{
		// Result array and array index are independent of which header we are extracting from
		$res = array();
		$i = 0;
		
		// This extracts recipients from the to header
		if (array_key_exists("to",$headers))
		{
			$address_array = explode(",",$headers["to"]);
			foreach($address_array as $address)
			{
				$sanitised_addr = "";
				$len = strlen($address);
				
				// We need to actively ignore everything until we hit the first '<' since that's where we'll find the mail address.
				// If no '<' exists then we must have a raw address so we can use that.
				$ignore = (strpos($address,"<"));
				for ($j = 0; $j < $len; $j++)
				{
					if ($ignore)
					{
						if ( $address[$j] === '<' )
						{
							$ignore = false;
						}
					}
					else if ( $address[$j] != '>' )
					{
						$sanitised_addr .= ( $address[$j] );
					}
				}
				
				// Add the address to the result array
				$res[$i] = str_replace(" ","",$sanitised_addr);
				$i++;
			}
		}
		
		// This extracts recipients from the cc header
		if (array_key_exists("cc",$headers))
		{
			$address_array = explode(",",$headers["cc"]);
			foreach($address_array as $address)
			{
				$sanitised_addr = "";
				$len = strlen($address);
				
				// We need to actively ignore everything until we hit the first '<' since that's where we'll find the mail address.
				// If no '<' exists then we must have a raw address so we can use that.
				$ignore = (strpos($address,"<"));
				for ($j = 0; $j < $len; $j++)
				{
					if ($ignore)
					{
						if ( $address[$j] === '<' )
						{
							$ignore = false;
						}
					}
					else if ( $address[$j] != '>' )
					{
						$sanitised_addr .= ( $address[$j] );
					}
				}
				
				// Add the address to the result array
				$res[$i] = str_replace(" ","",$sanitised_addr);
				$i++;
			}
		}
		
		return $res;
	}
}

/*
 * Extract config
 */

$bDebugMode = true;

if ($bDebugMode)
{
	echo "extracting config<br>\n";
}
 
$port = 0;
$imap_link = "";
$maildragon_inbox = "";
$maildragon_password = "";
$wiki_folder_root = "";
$maildragon_name = "";
$dokuwiki_gm_group = "gm";
$dokuwiki_mail_group = "mail";

// Go through line by line to get the settings
$config_file = file("./config.php");
foreach($config_file as $config_line)
{
	$config_exploded = explode(" ", $config_line);
	switch ($config_exploded[0])
	{
		case "port": $port = intval($config_exploded[1]); break;
		case "imap": $imap_link = trim($config_exploded[1]); break;
		case "inbox": $maildragon_inbox = trim($config_exploded[1]); break;
		case "password": $maildragon_password = trim($config_exploded[1]); break;
		case "wikifolderroot": $wiki_folder_root = trim($config_exploded[1]); break;
		case "maildragonname": $maildragon_name = trim($config_exploded[1]); break;
		case "dw_gmgroup": $dokuwiki_gm_group = trim($config_exploded[1]); break;
		case "dw_mailgroup": $dokuwiki_gm_group = trim($config_exploded[1]); break;
	}
}

if ( $port == 0 || $imap_link == "" || $maildragon_inbox == "" || $maildragon_password == "" || $wiki_folder_root == "" || $maildragon_name == "" )
{
	echo "INCORRECT CONFIG";
	exit;
}

/*
 * Create the MIGHTY IMAP DRIVER
 */

$imap_driver = new imap_driver();

/*
 * When we run majorritual.php we run a few tests to ensure that everything is working correctly
 */
// test for init()
if ($bDebugMode)
{
	echo "initialising...<br>\n";
}
	
if ($imap_driver->init($imap_link, $port) === false) {
    echo "init() failed: " . $imap_driver->error . "\n";
    exit;
}

// test for login()
if ($bDebugMode)
{
	echo "logging in...<br>\n";
}
if ($imap_driver->login($maildragon_inbox, $maildragon_password) === false) {
	echo "login() failed: " . $imap_driver->error . "\n";
	exit;
}

// test for select_folder()
if ($bDebugMode)
{
	echo "selecting INBOX...<br>\n";
}
if ($imap_driver->select_folder("INBOX") === false) {
    echo "select_folder() failed: " . $imap_driver->error . "\n";
    return false;
}

/*
 * Now let's get cracking!
 */

// get_uids_by_search() - ONLY SEARCH FOR UNSEEN MESSAGES
if ($bDebugMode)
{
	echo "getting uids for unseen messages...<br>\n";
}
$ids = $imap_driver->get_uids_by_search('UNSEEN');
if ($ids === false)
{
    echo "get_uids_failed: " . $imap_driver->error . "\n";
    exit;
}

// Go find our games.php file which contains a list of all our game names
if ($bDebugMode)
{
	echo "starting loop through games...<br>\n";
}
$games = file("./games.php");

// All code within the loop is specific to the game in question, everything else is free
foreach($games as $md_gamename)
{
	$gamename = trim($md_gamename);
	
	// I don't trust PHP to handle the last line with grace so have END here as a backstop
	if ($gamename == "END") break;
	
	if ($bDebugMode)
	{
		echo "Processing for " . $gamename . "...<br>\n";
	}
	
	// Get auth data - for DokuWiki this is always found at wikiroot/conf/users.auth.php
	$authdatafile = $wiki_folder_root . $gamename . "/conf/users.auth.php";
	if ($bDebugMode)
	{
		echo "finding authdata at " . $authdatafile . "...<br>\n";
	}
	$authdatalines = file($authdatafile);

	// Convert user auth data into something that we can parse with PHP
	// File format: login:passwordhash:Real Name:email:groups,comma,separated
	$usernames = array();
	$emails = array();
	$gm = array(); // To be a bitmap of whether users are GMs for this game
	$pc = array(); // To be a bitmap of whether users are PCs for this game
	
	// TODO we really need to handle GROUPS with the default MailDragon implementation
	
	// Each user will be referred to by ID while we're extracting data - we'll convert that into a name->details mapping below
	$userindex = 0;

	if ($bDebugMode)
	{
		echo "reading authdata<br>\n";
	}
	foreach($authdatalines as $line)
	{
		if ($line[0] != "#")
		{
			$boom = explode(":", $line);
			// Break up so we have:
			//  login:passwordhash:Real Name:email:groups,comma,separated
			if ($boom != FALSE && count($boom) == 5) 
			{
				$usernames[$userindex] = $boom[0];
				$emails[$userindex] = $boom[3];
				$gm[$userindex] = false;
				$pc[$userindex] = false;
				
				$groups = explode(",", $boom[4]);
				foreach ($groups as $group)
				{
					$group = preg_replace("/[^A-Za-z]/","",$group);
					if ($group == $dokuwiki_gm_group)
					{
						$gm[$userindex] = true;
					}
					if ($group == $dokuwiki_mail_group)
					{
						$pc[$userindex] = true;
					}
				}
				++$userindex;
			}
		}
	}

	// Build mapping of username (IC name) to email address (OC address).  We will also add these addresses to the GMs array if the user is a GM.
	if ($bDebugMode)
	{
		echo "constructing mapping...<br>\n";
	}
	$mapping = array();
	$gms = array();
	$inumgms = 0;
	for ($iuser = 0; $iuser < $userindex; $iuser++)
	{
		if ($pc[$iuser])
		{
			$mapping[$usernames[$iuser]] = $emails[$iuser];
		}
		if ($gm[$iuser])
		{
			$gms[$inumgms] = $emails[$iuser];
			$inumgms++;
		}
	}

	// Construct the GM Bcc which will be used for all emails - we basically concatenate all those GM addresses comma separated
	$gm_bcc = "";

	if ($bDebugMode)
	{
		echo "constructing gm bcc...<br>\n";
	}

	for ($i = 0; $i < $inumgms; $i++)
	{
		$gm_bcc .= $gms[$i];
		if ($i < $inumgms - 1)
		{
			$gm_bcc .= ",";
		}
	}

	if ($bDebugMode)
	{
		echo "GM bcc: " . $gm_bcc . "<br>\n";
	}

	// Go through the inbox and process each mail
	// TODO - Upgrade this to use mail relay not sending!
	for ($index=0; $index < count($ids); $index++)
	{
		$uid = $ids[$index];

		// Create the extractor
		$maildragon = new MailDragonExtractor();
		
		// Extract the from address
		$from = $maildragon->extract_from_mail_address($imap_driver->get_headers_from_uid($uid));
		
		// Don't do anything with emails I sent!
		if (strpos($from, $maildragon_name) !== false)
		{
			$imap_driver->set_seen($uid);
			continue;
		}
		
		// Get the email headers!
		$headers = $imap_driver->get_headers_from_uid($uid);
		
		// Set up constant data for this email copy being sent
		// TODO - Attachments ( may be covered by relaying )
		$mimeversion = $headers["mime-version"];
		$subject = $headers["subject"];
		$contenttype = $headers["content-type"];
		
		$text = $imap_driver->get_text_from_uid($uid);
		
		$bExternalAddress = true;
		$from_pc = "Unrecognised Character";
		
		// Find the sender character name
		foreach ($mapping as $character => $oc_email)
		{
			if ($oc_email == $from)
			{
				$from_pc = $character;
				$bExternalAddress = false;
			}
		}
		
		// Construct from and reply-to fields
		$fromfield = $bExternalAddress ? $from : ucwords($from_pc) . '<' . $maildragon_inbox . '>';
		$replytofield = $bExternalAddress ? $from : ucwords($from_pc) . ' <' . $from_pc . '@' . $gamename . '.oxfordrpg.com>';
		
		// Array of all the PC Emails.  For each one, we need to extract the OC email address and send to that, but
		// Cc in all of the other PC Email addresses
		$to_pc_emails_array = $maildragon->extract_to_mail_addresses($headers);
		
		if (count($to_pc_emails_array) == 0)
		{
			continue;
		}
		
		$to_pcs = array();
		$inumtopcs = 0;
		
		$num_to_pc_emails = count($to_pc_emails_array);
		$gm_targeted = false;
		
		// Loop through all the PCs we want to send to, construct the email and send it on
		for ($i = 0; $i < $num_to_pc_emails; $i++)
		{
			$to_pc_email = $to_pc_emails_array[$i];
			
			if (strpos($to_pc_email, $gamename) === false)
			{
				continue;
			}
			
			$explodedemail = explode("@", $to_pc_email);
			
			// This is the recipient we're currently sending for
			$to_pc = $explodedemail[0]; // Get the bit before the @
			
			// Construct the Cc for everyone else
			$cc = "";
			
			for ($j = 0; $j < $num_to_pc_emails; $j++)
			{
				if ($i != $j)
				{
					$cc .= $to_pc_emails_array[$j];
					if ($j < $num_to_pc_emails - 1 || $j + 1 != $i)
					{
						$cc .= ",";
					}
				}
			}
			
			$to_address = "";
			
			// Find the recipient OC email
			foreach ($mapping as $character => $oc_email)
			{			
				if ($character == $to_pc)
				{
					$to_address = $oc_email;
				}
			}
			
			if (in_array($to_address,$gms))
			{
				// This is a GM - skip!
				$gm_targeted = true;
				continue;
			}
			
			echo "<br>To: " . $to_address;
			echo "<br>Cc: " . $cc;
				
			$a = array( 'From' => $fromfield,
				'Reply-To' => $replytofield,
				'Content-type' => $contenttype,
				'MIME-Version' => $mimeversion);
			
			mail ( $to_address, $subject, $text, $a );
			
			echo "<br>Sent!";	
			++$inumtopcs;
		}
		
		// Send the GM copy if we sent any PC emails
		
		if ($inumtopcs > 0 || $gm_targeted)
		{
			$a = array( 'From' => $fromfield,
					'Reply-To' => $replytofield,
					'Content-type' => $contenttype,
					'MIME-Version' => $mimeversion,
					'Bcc' => $gm_bcc );
				
			mail( $maildragon_inbox, $subject, $text, $a );
			
			echo "<br>Sent GM copy!";
			
			// Set seen
			$imap_driver->set_seen($uid);		
			
			echo "<br>Email handled - " . $subject;
			echo "<br>";
			echo "/n";
		}		
	}
}


?>